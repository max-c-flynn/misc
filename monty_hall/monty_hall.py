# Max Flynn 2022
# Demonstration of the "Monty Hall" problem, a sort've logic paradox
#
#      If one is on a game show where there is a prize behind one of three doors
#      and you choose exactly one of the doors with no knowledge
#      and the game show host reveals one of the blank doors for you,
#      you are MORE likely to win the prize if you elect to change your choice
#      of door rather than elect to "stay-put".

# While this can be demonstrated with combinatorics, we'll demonstrate this stochastically instead.
# The exact answer is you are 2/3 likely to win if you switch rather than if you stay-put.

import sys
from random import randrange

if (len(sys.argv) > 1):
    N_TRIALS = int(sys.argv[1])
else:
    N_TRIALS = 1000 # default

success_on_switch = 0 # tally

if __name__ == "__main__":
    
    print(sys.argv)
    
    # this could be done faster by generating a matrix of all results at once, at the cost of import numpy
    for trial in range(N_TRIALS):
        
        # set the index of the prize-door and choice
        prize_idx = randrange(3) # selects one of 0,1,2
        choice_idx = randrange(3)
        
        #print("Prize is index: {}".format(prize_idx))
        #print("User chose index: {}".format(choice_idx))
        
        #monty_idx = list([0,1,2])
        
        #monty_idx.pop(choice_idx) # remove the door the user chose
        #monty_idx.pop(randrange(2)) # randomly remove either index 0 or index 1
        #print("Monty reveals door number: {}".format(monty_idx))
        
        # one interesting aspect, you do not need to do *any* monty-math to show the real result
        if prize_idx != choice_idx:
            success_on_switch = success_on_switch+1
        
    print("The true result is 2/3, or 0.66")
    print("Given by {} trials, demonstrated success rate: {}".format(N_TRIALS, success_on_switch / N_TRIALS))