# Max Flynn 2022
# Demonstration of a stochastic way to find the value of pi
# by relating the area between a square and a circumscribed circle.
# The area is estimated by throwing numbers of darts

# If a square is known to have area N^2,
# and we presume a circle has area pi*(N/2)^2 where pi is unknown,
# then the ratio of a_circ / a_square yields pi if the circle is circumscribed within

import random
import sys

SIDE = 1

if (len(sys.argv) > 1):
    N_TRIALS = int(sys.argv[1])
else:
    N_TRIALS = 10000

n_in_circle = 0        
        
def is_in_circle(dart_x, dart_y):
    # definition of the inside of a circle: x^2 + y^2 <= 1
    if (dart_x*dart_x + dart_y*dart_y) < (SIDE/2)*(SIDE/2):
        #print("left hand side: {}".format(dart_x*dart_x + dart_y*dart_y))
        return True
        
        
        

if __name__ == "__main__":
    
    for trial in range(N_TRIALS):
        
        dart_x = random.random()-0.5; # uniform random number in range [-0.5, 0.5]
        dart_y = random.random()-0.5;
        
        #print("{}, {}".format(dart_x, dart_y))
        
        # to save iterations, we can say every single dart will hit inside the square
        # so we only need to count those inside the circle
        if is_in_circle(dart_x, dart_y):
            n_in_circle = n_in_circle + 1
        
        
    print("Final estimated value of pi using {} trials: {}".format(N_TRIALS, n_in_circle * 4 / N_TRIALS))